package ru.ekfedorov.tm.bootstrap;

import ru.ekfedorov.tm.api.ICommandController;
import ru.ekfedorov.tm.api.ICommandRepository;
import ru.ekfedorov.tm.api.ICommandService;
import ru.ekfedorov.tm.constant.ArgumentConst;
import ru.ekfedorov.tm.constant.TerminalConst;
import ru.ekfedorov.tm.controller.CommandController;
import ru.ekfedorov.tm.repository.CommandRepository;
import ru.ekfedorov.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    public final ICommandRepository commandRepository = new CommandRepository();

    public final ICommandService commandService = new CommandService(commandRepository);

    public final ICommandController commandController = new CommandController(commandService);

    public void run(final String... args) {
        displayWelcome();
        if (parseArgs(args)) commandController.exit();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            System.out.println();
            parseCommand(command);
        }
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default: showIncorrectArgument();
        }
    }

    private void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            default: showIncorrectCommand();
        }
    }

    private void showIncorrectCommand() {
        System.out.println("Error! Command not found...\n");
    }

    private void showIncorrectArgument() {
        System.out.println("Error! Argument not found...");
    }

    private boolean parseArgs(final String... args) {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        parseArg(param);
        return true;
    }

}
