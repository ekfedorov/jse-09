package ru.ekfedorov.tm.service;

import ru.ekfedorov.tm.api.ICommandRepository;
import ru.ekfedorov.tm.api.ICommandService;
import ru.ekfedorov.tm.model.Command;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}

